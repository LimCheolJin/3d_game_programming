#include "stdafx.h"
#include "Scene.h"
#include "Player.h"
#include <iostream>
#include <random>
using namespace std;
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
normal_distribution<> nd{500.0 , 1000.0 };
uniform_int_distribution<> uidColor{0, 255};
default_random_engine dre;

CScene::CScene()
{
	m_pCubeMesh = new CCubeMesh(5.0f, 0.0f, 10.0f);
	m_pTreeMesh = new CTreeMesh(4.0f, 4.0f, 4.0f);
	m_pStarMesh = new CStarMesh(4.0f, 4.0f, 4.0f);
}

CScene::~CScene()
{
}

void CScene::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
}

void CScene::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
}

void CScene::BuildObjects(CPlayer *pPlayer)
{
}

void CScene::ReleaseObjects()
{
}

void CScene::CheckObjectByObjectCollisions()
{
	for (int i = 0; i < m_nObjects; i++) m_ppObjects[i]->m_pObjectCollided = NULL;
	for (int i = 0; i < m_nObjects; i++)
	{
		for (int j = (i + 1); j < m_nObjects; j++)
		{
			if (m_ppObjects[i]->m_xmOOBB.Intersects(m_ppObjects[j]->m_xmOOBB))
			{
				m_ppObjects[i]->m_pObjectCollided = m_ppObjects[j];
				m_ppObjects[j]->m_pObjectCollided = m_ppObjects[i];
			}
		}
	}
	for (int i = 0; i < m_nObjects; i++)
	{
		if (m_ppObjects[i]->m_pObjectCollided)
		{
			XMFLOAT3 xmf3MovingDirection = m_ppObjects[i]->m_xmf3MovingDirection;
			float fMovingSpeed = m_ppObjects[i]->m_fMovingSpeed;
			m_ppObjects[i]->m_xmf3MovingDirection = m_ppObjects[i]->m_pObjectCollided->m_xmf3MovingDirection;
			m_ppObjects[i]->m_fMovingSpeed = m_ppObjects[i]->m_pObjectCollided->m_fMovingSpeed;
			m_ppObjects[i]->m_pObjectCollided->m_xmf3MovingDirection = xmf3MovingDirection;
			m_ppObjects[i]->m_pObjectCollided->m_fMovingSpeed = fMovingSpeed;
			m_ppObjects[i]->m_pObjectCollided->m_pObjectCollided = NULL;
			m_ppObjects[i]->m_pObjectCollided = NULL;
		}
	}
}

void CScene::FindMaxDistanceObject(CGameObject* object1,CGameObject* object2)
{
	
}

XMFLOAT3& CScene::SubtractObjects(CGameObject* object, CPlayer* pPlayer)
{
	
	XMFLOAT3 dir = Vector3::Subtract(object->GetPosition(), pPlayer->GetPosition(), false);

	
	return dir;
	
}
void CScene::Animate(float fElapsedTime)
{

}
void CScene::CreateObject(float fElapsedTime, CPlayer* pPlayer)
{
	if (Floorcnt >= 500)
	{
		Floorcnt = 0;
		for (int i = 0; i < 500; ++i)
		{
			m_ppObjects[i]->m_bPass = false;
			m_ppObjects[i]->SetColor(RGB(255, 255, 255));
			m_ppObjects[i]->m_fPassTime = 0.0f;
		}
	}
	if (Floorcnt == 0) 
	{
		m_ppObjects[Floorcnt]->SetColor(RGB(0, 0, 0));
		m_ppObjects[Floorcnt]->SetLookVector(pPlayer->GetLook());
		m_ppObjects[Floorcnt]->SetRightVector(pPlayer->GetRight());
		m_ppObjects[Floorcnt]->SetUpVector(pPlayer->GetUp());
		m_ppObjects[Floorcnt]->Rotate(-90.0f, 0.0f, 0.0f);
		m_ppObjects[Floorcnt]->SetPosition(pPlayer->GetPosition());
		Floorcnt++;
	}
	if (Vector3::Distance(pPlayer->GetPosition(), m_ppObjects[(Floorcnt-1)]->GetPosition() ) > 3.0f)
	{
		m_ppObjects[(Floorcnt - 1) % m_nObjects]->SetColor(RGB(255, 0, 0));
		m_ppObjects[Floorcnt ]->SetColor(RGB(0, 0, 0));
		m_ppObjects[Floorcnt]->SetLookVector(pPlayer->GetLook());
		m_ppObjects[Floorcnt]->SetRightVector(pPlayer->GetRight());
		m_ppObjects[Floorcnt]->SetUpVector(pPlayer->GetUp());
		m_ppObjects[Floorcnt]->Rotate(-90.0f, 0.0f, 0.0f);
		m_ppObjects[Floorcnt ]->SetPosition(pPlayer->GetPosition());
		m_ppObjects[Floorcnt]->m_bPass = true;
		Floorcnt++;
	}
	
	for (int i = 0; i < 500; ++i)
		m_ppObjects[i]->Animate(fElapsedTime, pPlayer);
	
	if (pPlayer->GetPosition().z >= m_ppObjects[545]->GetPosition().z)
	{
		for (int i = 500; i < 620; ++i)
		{
			if (i >= 500 && i < 560)
				m_ppObjects[i]->SetPosition(-10.0f, -30.0f, pPlayer->GetPosition().z + i * 8.0f - 4000.0f);
			else if (i >= 560 && i < 620)
				m_ppObjects[i]->SetPosition(10.0f, -30.0f, pPlayer->GetPosition().z + (i - 60) * 8.0f - 4000.0f);
		}
	}
	for (int i = 620; i < 1000; ++i)
		m_ppObjects[i]->SetColor(RGB(uidColor(dre), uidColor(dre), uidColor(dre)));
}
void CScene::Animate(float fElapsedTime, CPlayer *pPlayer)
{
	
}

void CScene::Render(HDC hDCFrameBuffer, CCamera *pCamera)
{
	for (int i = 0; i < m_nObjects; i++)
	{
		if (m_ppObjects[i]) {
			m_ppObjects[i]->Render(hDCFrameBuffer, pCamera);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
CPlayerScene::CPlayerScene()
{
}

CPlayerScene::~CPlayerScene()
{
}

void CPlayerScene::BuildObjects(CPlayer *pPlayer)
{
	
	//m_nObjects = 620;
	m_nObjects = 1000;
	m_ppObjects = new CGameObject*[m_nObjects];
	for (int i = 0; i < m_nObjects; ++i)
	{
		m_ppObjects[i] = new CGameObject();
		if (i < 500)
		{
			m_ppObjects[i]->SetMesh(m_pCubeMesh);
			//m_ppObjects[i]->SetPosition(50000.0f, 50000.0f, 50000.0f);
		}
		if (i >= 500 && i < 560)
		{
			m_ppObjects[i]->SetMesh(m_pTreeMesh);
			m_ppObjects[i]->SetColor(RGB(0, 255, 0));
			m_ppObjects[i]->SetPosition(-10.0f, -30.0f, i * 8.0f - 4000.0f);
		}
		else if (i >= 560 && i < 620)
		{
			m_ppObjects[i]->SetMesh(m_pTreeMesh);
			m_ppObjects[i]->SetColor(RGB(0, 255, 0));
			m_ppObjects[i]->SetPosition(10.0f, -30.0f, (i - 60) * 8.0f - 4000.0f);
		}

		if (i >= 620 && i < 1000)
		{
			m_ppObjects[i]->SetMesh(m_pStarMesh);
			m_ppObjects[i]->SetPosition(nd(dre), nd(dre), nd(dre));
		}
	}
}

