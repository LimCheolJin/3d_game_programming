#pragma once
#include "GameObject.h"
//#include "Player.h"

typedef struct Angle
{
	float x;
	float y;
}Angle;

class CPlayer;
class CScene
{
public:
	CScene();
	virtual ~CScene();

	CPlayer						*m_pPlayer = NULL;
	int							m_nObjects = 0;
	CGameObject					**m_ppObjects = NULL;
	CGameObject					*m_MaxObjects = NULL;
	
public:
	XMFLOAT3& SubtractObjects(CGameObject* object, CPlayer* pPlayer);
	void FindMaxDistanceObject(CGameObject* object1, CGameObject *object2);
	virtual void BuildObjects(CPlayer *pPlayer);
	virtual void ReleaseObjects();

	virtual void CheckObjectByObjectCollisions();
		
	virtual void Animate(float fElapsedTime, CPlayer* pPlayer = NULL);
	virtual void Render(HDC hDCFrameBuffer, CCamera *pCamera);
	virtual void Animate(float fElapsedTime);
	virtual void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	virtual void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	//
	virtual void CreateObject(float fElapsedTime, CPlayer* pPlayer = NULL);
	int Floorcnt = 0;
	bool m_bStart = true;
	CMesh						*m_pCubeMesh = NULL;
	Angle						*m_StoreRotate = NULL;
	CTreeMesh					*m_pTreeMesh = NULL;
	CStarMesh					*m_pStarMesh = NULL;
	bool						m_bCreateTree = true;
};

class CPlayerScene : public CScene
{
public:
	CPlayerScene();
	virtual ~CPlayerScene();
	virtual void BuildObjects(CPlayer *pPlayer);
};

