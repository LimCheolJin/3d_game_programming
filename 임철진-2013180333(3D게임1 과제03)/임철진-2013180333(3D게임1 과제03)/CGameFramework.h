#pragma once
#include "Timer.h"
#include "Scene.h"
#include "Camera.h"
#include "Player.h"
class CGameFramework
{
private:
	HINSTANCE m_hInstance;
	HWND m_hWnd;

	int m_nWndClientWidth;
	int m_nWndClientHeight;

	IDXGIFactory4 *m_pdxgiFactory; // DXGI 팩토리 인터페이스에 대한 포인터.
	IDXGISwapChain3 *m_pdxgiSwapChain; // 스왑체인 인터페이스에 대한 포인터. (주로 디스플레이 제어위해 필요)
	ID3D12Device *m_pd3dDevice; // Direct3D 디바이스 인터페이스에 대한 포인터. 주로 리소스를 생성하기위해 필요.

	bool m_bMsaa4xEnable = false;
	UINT m_nMsaa4xQualityLevels = 0;
	//MSAA 다중 샘플링을 활성화하고 다중 샘플링 레벨을 설정한다.

	static const UINT m_nSwapChainBuffers = 2; // 스왑체인의 후면버퍼의 개수.
	UINT m_nSwapChainBufferIndex;  // 현재 스왑체인의 후면버퍼 인덱스.
	ID3D12Resource *m_ppd3dRenderTargetBuffers[m_nSwapChainBuffers];
	ID3D12DescriptorHeap *m_pd3dRtvDescriptorHeap;
	UINT m_nRtvDescriptorIncrementSize;
	// 랜더타겟버퍼, 서술자 힙 인터페이스 포인터, 랜더타켓서술자원소의크기
	ID3D12Resource *m_pd3dDepthStencilBuffer;
	ID3D12DescriptorHeap *m_pd3dDsvDescriptorHeap;
	UINT m_nDsvDescriptorIncrementSize;
	//깊이-스텐실버퍼, 서술자 힙 인터페이스 포인터, 깊이-스텐실 서술자 원소의 크기
	ID3D12CommandQueue *m_pd3dCommandQueue;
	ID3D12CommandAllocator *m_pd3dCommandAllocator;
	ID3D12GraphicsCommandList *m_pd3dCommandList;
	//명령 큐, 명령 할당자, 명령 리스트 인터페이스 포인터이다.
	ID3D12Fence *m_pd3dFence;
	UINT64 m_nFenceValues[m_nSwapChainBuffers]; //06에서 수정된부분 , 후면버퍼마다 펜스 값을 관리하기 위해 수정함.
	HANDLE m_hFenceEvent;
	//펜스 인터페이스 포인터, 펜스의 값, 이벤트 핸들이다.
	#if defined(_DEBUG)
		ID3D12Debug *m_pd3dDebugController;
	#endif
	CGAMETimer m_GameTimer; //게임프레임워크에서 사용할 타이머.
	_TCHAR m_pszFrameRate[50]; //프레임레이트를 윈도우의 캡션에 출력하기 위한 문자열.

	CScene *m_pScene; // 씬을 위한 멤버변수
public:
	CGameFramework();
	~CGameFramework();

	bool OnCreate(HINSTANCE hInstance, HWND hMainWnd);
	//프레임워크를 초기화 하는 함수이다.(주 윈도우가 생성되면 호출됨)
	void OnDestroy();

	void CreateSwapChain(); 
	void CreateDirect3DDevice();
	void CreateRtvAndDsvDescriptorHeaps(); 
	void CreateCommandQueueAndList();
	void CreateRenderTargetView();
	void CreateDepthStencilView();
	//스왑체인, 디바이스, 서술자 힙, 명령 큐/할당자/리스트를 생성하는 함수.
	void BuildObjects();
	void ReleaseObjects();
	//렌더링할 메쉬와 게임 객체를 생성하고 소멸하는 함수이다.
	
	//////////////////////////////////////////////////////////////////////
	void ProcessInput();
	void AnimateObjects();
	//프레임 워크의 핵심(사용자입력, 애니메이션,렌더링) 을 구성하는 함수.

	void FrameAdvance();
	void WaitForGpuComplete();
	//cpu와 gpu를 동기화 하는 함수.

	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	// 윈도우의 메세지(키보드,마우스입력)을 처리하는 함수이다.

public:
	void OnResizeBackBuffers();

public:
	void MoveToNextFrame();

public:
	CCamera *m_pCamera = NULL;

public:
	//플레이어 객체에 대한 포인터이다. 
	CPlayer *m_pPlayer = NULL;
	//마지막으로 마우스 버튼을 클릭할 때의 마우스 커서의 위치이다.
	POINT m_ptOldCursorPos;    
};

