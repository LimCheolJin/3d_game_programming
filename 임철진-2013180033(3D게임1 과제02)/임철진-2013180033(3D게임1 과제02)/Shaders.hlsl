
//게임 객체의 정보를 위한 상수 버퍼를 선언한다.
cbuffer cbGameObjectInfo : register(b0) 
{ 
	matrix gmtxWorld;  //: packoffset(c0);
	float4 Color;
};

//카메라의 정보를 위한 상수 버퍼를 선언한다.
cbuffer cbCameraInfo : register(b1) 
{ 
	matrix gmtxView : packoffset(c0);
	matrix gmtxProjection : packoffset(c4);
};

//정점 셰이더의 입력을 위한 구조체를 선언한다. 
struct VS_INPUT
{ 
	float3 position : POSITION; 
	float4 color : COLOR; 
};

//정점 셰이더의 출력(픽셀 셰이더의 입력)을 위한 구조체를 선언한다. 
struct VS_OUTPUT 
{
	float4 position : SV_POSITION; 
	float4 color : COLOR; 
};

//정점 셰이더를 정의한다.
VS_OUTPUT VSDiffused(VS_INPUT input) 
{
	VS_OUTPUT output; // 정점을 변환 한다. (월드변환, 카메라변환, 투영변환)
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	input.color = Color;
	output.color = input.color;
	return(output);
}

//픽셀 셰이더를 정의한다.
float4 PSDiffused(VS_OUTPUT input) : SV_TARGET
{ 
	return(input.color); 
}


// player
VS_OUTPUT VSPlayer(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	output.color = input.color;
	return(output);
}

float4 PSPlayer(VS_OUTPUT input) : SV_TARGET
{
   return(input.color);
}


//Tree
VS_OUTPUT VSTree(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	output.color = input.color; // 메쉬의 색깔값을사용할것임.
	return(output);
}

float4 PSTree(VS_OUTPUT input) : SV_TARGET
{
   return(input.color);
}

//star
VS_OUTPUT VSStar(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	input.color = Color;
	output.color = input.color;
	return(output);
}

float4 PSStar(VS_OUTPUT input) : SV_TARGET
{
   return(input.color);
}

//빌딩
VS_OUTPUT VSBuilding(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	input.color = Color;
	output.color = input.color;
	return(output);
}

float4 PSBuilding(VS_OUTPUT input) : SV_TARGET
{
   return(input.color);
}