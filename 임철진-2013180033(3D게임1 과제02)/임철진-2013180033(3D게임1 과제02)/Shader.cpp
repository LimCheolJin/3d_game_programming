#include "stdafx.h"
#include "Shader.h"
#include "Player.h"
#include <iostream>
#include <random>
using namespace std;
uniform_real_distribution<> urd(0.0f, 1.0f);
default_random_engine dreColorBuilding;

CShader::CShader()
{
}


CShader::~CShader()
{
	if (m_ppd3dPipelineStates) 
	{ 
		for (int i = 0; i < m_nPipelineStates; i++) 
			if (m_ppd3dPipelineStates[i])
				m_ppd3dPipelineStates[i]->Release();
		delete[] m_ppd3dPipelineStates; 
	}
}

D3D12_RASTERIZER_DESC CShader::CreateRasterizerState()
{
	D3D12_RASTERIZER_DESC d3dRasterizerDesc; 
	::ZeroMemory(&d3dRasterizerDesc, sizeof(D3D12_RASTERIZER_DESC)); 
	d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	//d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
	d3dRasterizerDesc.FrontCounterClockwise = FALSE; 
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f; 
	d3dRasterizerDesc.DepthClipEnable = TRUE;
	d3dRasterizerDesc.MultisampleEnable = FALSE;
	d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
	d3dRasterizerDesc.ForcedSampleCount = 0; 
	d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
	return(d3dRasterizerDesc);

}



D3D12_DEPTH_STENCIL_DESC CShader::CreateDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC d3dDepthStencilDesc; 
	::ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	d3dDepthStencilDesc.DepthEnable = TRUE; ///////깊이값을 사용할것인지.
	d3dDepthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	d3dDepthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	d3dDepthStencilDesc.StencilEnable = FALSE;
	d3dDepthStencilDesc.StencilReadMask = 0x00; 
	d3dDepthStencilDesc.StencilWriteMask = 0x00; 
	d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP; 
	d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP; 
	d3dDepthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER; 
	d3dDepthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP; 
	d3dDepthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP; 
	d3dDepthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	return(d3dDepthStencilDesc);
}

D3D12_BLEND_DESC CShader::CreateBlendState()
{
	D3D12_BLEND_DESC d3dBlendDesc; 
	::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
	d3dBlendDesc.AlphaToCoverageEnable = FALSE;
	d3dBlendDesc.IndependentBlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].BlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD; 
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE; 
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD; 
	d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(d3dBlendDesc);

}

D3D12_INPUT_LAYOUT_DESC CShader::CreateInputLayout()
{
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = NULL;
	d3dInputLayoutDesc.NumElements = 0;
	return(d3dInputLayoutDesc);

}

//정점 셰이더 바이트 코드를 생성(컴파일)한다. 
D3D12_SHADER_BYTECODE CShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob) 
{ 
	D3D12_SHADER_BYTECODE d3dShaderByteCode;
	d3dShaderByteCode.BytecodeLength = 0; 
	d3dShaderByteCode.pShaderBytecode = NULL;

	return(d3dShaderByteCode);

}

//픽셀 셰이더 바이트 코드를 생성(컴파일)한다. 
D3D12_SHADER_BYTECODE CShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob)
{
	D3D12_SHADER_BYTECODE d3dShaderByteCode; 
	d3dShaderByteCode.BytecodeLength = 0; 
	d3dShaderByteCode.pShaderBytecode = NULL;

	return(d3dShaderByteCode);
}

//셰이더 소스 코드를 컴파일하여 바이트 코드 구조체를 반환한다. 
D3D12_SHADER_BYTECODE CShader::CompileShaderFromFile(WCHAR *pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderProfile, ID3DBlob **ppd3dShaderBlob)
{
	UINT nCompileFlags = 0;
#if defined(_DEBUG)
	nCompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	ID3DBlob* errorBlob = nullptr;

	::D3DCompileFromFile(pszFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, pszShaderName, pszShaderProfile, nCompileFlags, 0, ppd3dShaderBlob, &errorBlob);

	if (errorBlob)
	{
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());
		errorBlob->Release();
	}


	D3D12_SHADER_BYTECODE d3dShaderByteCode;
	d3dShaderByteCode.BytecodeLength = (*ppd3dShaderBlob)->GetBufferSize();
	d3dShaderByteCode.pShaderBytecode = (*ppd3dShaderBlob)->GetBufferPointer();

	return(d3dShaderByteCode);
}

//그래픽스 파이프라인 상태 객체를 생성한다. 
void CShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
	//그래픽스 파이프라인 상태 객체 배열을 생성한다. 
	ID3DBlob *pd3dVertexShaderBlob = NULL, *pd3dPixelShaderBlob = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC d3dPipelineStateDesc; 
	::ZeroMemory(&d3dPipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	d3dPipelineStateDesc.pRootSignature = pd3dGraphicsRootSignature;
	d3dPipelineStateDesc.VS = CreateVertexShader(&pd3dVertexShaderBlob); 
	d3dPipelineStateDesc.PS = CreatePixelShader(&pd3dPixelShaderBlob);
	d3dPipelineStateDesc.RasterizerState = CreateRasterizerState(); 
	d3dPipelineStateDesc.BlendState = CreateBlendState(); 
	d3dPipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	d3dPipelineStateDesc.InputLayout = CreateInputLayout(); 
	d3dPipelineStateDesc.SampleMask = UINT_MAX;
	d3dPipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE; 
	d3dPipelineStateDesc.NumRenderTargets = 1; 
	d3dPipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM; 
	d3dPipelineStateDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dPipelineStateDesc.SampleDesc.Count = 1;
	d3dPipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	pd3dDevice->CreateGraphicsPipelineState(&d3dPipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&m_ppd3dPipelineStates[0]);

	if (pd3dVertexShaderBlob) 
		pd3dVertexShaderBlob->Release();
	if (pd3dPixelShaderBlob) 
		pd3dPixelShaderBlob->Release();
	if (d3dPipelineStateDesc.InputLayout.pInputElementDescs)
		delete[] d3dPipelineStateDesc.InputLayout.pInputElementDescs;
}
void CShader::OnPrepareRender(ID3D12GraphicsCommandList *pd3dCommandList)
{ //파이프라인에 그래픽스 상태 객체를 설정한다. 
	pd3dCommandList->SetPipelineState(m_ppd3dPipelineStates[0]);
}

void CShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
	OnPrepareRender(pd3dCommandList);
}

void CShader::CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
	
}

void CShader::UpdateShaderVariables(ID3D12GraphicsCommandList *pd3dCommandList) 
{

}

void CShader::UpdateShaderVariable(ID3D12GraphicsCommandList *pd3dCommandList, XMFLOAT4X4 *pxmf4x4World)
{ 
	XMFLOAT4X4 xmf4x4World;
	XMStoreFloat4x4(&xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(pxmf4x4World)));
	pd3dCommandList->SetGraphicsRoot32BitConstants(0, 16, &xmf4x4World, 0); 
}

void CShader::ReleaseShaderVariables() 
{ 

}





///////////////CObjectsShader///////////////////////////
CObjectsShader::CObjectsShader() 
{

}
CObjectsShader::~CObjectsShader() 
{

}

D3D12_INPUT_LAYOUT_DESC CObjectsShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2; 
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc; 
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);

}

D3D12_SHADER_BYTECODE CObjectsShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSBuilding", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CObjectsShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSBuilding", "ps_5_1", ppd3dShaderBlob)); 
}

void CObjectsShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
	m_nPipelineStates = 1;
	m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CObjectsShader::BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
	CreateShaderVariables(pd3dDevice, pd3dCommandList);
}

void CObjectsShader::ReleaseObjects()
{
	if (m_ppObjects) 
	{ 
		for (int j = 0; j < m_nObjects; j++) 
		{ 
			if (m_ppObjects[j]) 
				delete m_ppObjects[j]; 
		}
		delete[] m_ppObjects; 
	}

}
void CObjectsShader::AnimateObjects(float fTimeElapsed, CPlayer *pPlayer)
{
	for (int j = 0; j < m_nObjects; j++)
		m_ppObjects[j]->Animate(fTimeElapsed, pPlayer);
}

void CObjectsShader::ReleaseUploadBuffers()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++) 
			m_ppObjects[j]->ReleaseUploadBuffers();
	}
}
void CObjectsShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
	CShader::Render(pd3dCommandList, pCamera);
}




///////////////CFloorShader///////////////////////////
CFloorShader::CFloorShader()
{

}
CFloorShader::~CFloorShader()
{

}

D3D12_INPUT_LAYOUT_DESC CFloorShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);

}

D3D12_SHADER_BYTECODE CFloorShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSDiffused", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CFloorShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSDiffused", "ps_5_1", ppd3dShaderBlob));
}

void CFloorShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
	m_nPipelineStates = 1;
	m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CFloorShader::BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
	CFloorMeshDiffused *pFloorMesh = new CFloorMeshDiffused(pd3dDevice, pd3dCommandList, 20.0f, -6.0f, 15.0f); // (12.0f,12.0f,12.0f)

	m_nObjects = 1000;
	m_ppObjects = new CGameObject*[m_nObjects];

	CFloorObject *pFloorObject = NULL;


	for (int i = 0; i < 1000; ++i)
	{
		pFloorObject = new CFloorObject();
		pFloorObject->SetMesh(pFloorMesh);
		
		pFloorObject->FloorColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
		pFloorObject->SetPosition(50000.0f, 50000.0f, 50000.0f);

		m_ppObjects[i] = pFloorObject;
	}

	CreateShaderVariables(pd3dDevice, pd3dCommandList);

}

void CFloorShader::ReleaseObjects()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
		{
			if (m_ppObjects[j])
				delete m_ppObjects[j];
		}
		delete[] m_ppObjects;
	}

}
void CFloorShader::AnimateObjects(float fTimeElapsed, CPlayer *pPlayer)
{
	for (int j = 0; j < m_nObjects; j++)
	{
		m_ppObjects[j]->Animate(fTimeElapsed, pPlayer);
	}
	if (Floorcnt >= 1000)
		Floorcnt = 0;
	if (Floorcnt == 0)
	{
		m_ppObjects[Floorcnt]->SetLookVector(pPlayer->GetLook());
		m_ppObjects[Floorcnt]->SetRightVector(pPlayer->GetRight());
		m_ppObjects[Floorcnt]->SetUpVector(pPlayer->GetUp());
		m_ppObjects[Floorcnt]->SetPosition(pPlayer->GetPosition().x, pPlayer->GetPosition().y, pPlayer->GetPosition().z);
		m_ppObjects[Floorcnt]->Rotate(-90.0f, 0.0f, 0.0f);
		m_ppObjects[Floorcnt]->FloorColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
		dynamic_cast<CFloorObject*>(m_ppObjects[Floorcnt])->m_IsRender = true;
		Floorcnt++;
	}
	if (Vector3::Distance(pPlayer->GetPosition(), m_ppObjects[(Floorcnt - 1)]->GetPosition()) < 5.0f)
	{
		m_ppObjects[Floorcnt]->SetLookVector(pPlayer->GetLook());
		m_ppObjects[Floorcnt]->SetRightVector(pPlayer->GetRight());
		m_ppObjects[Floorcnt]->SetUpVector(pPlayer->GetUp());
		m_ppObjects[Floorcnt]->SetPosition(pPlayer->GetPosition());
		m_ppObjects[Floorcnt]->Rotate(-90.0f, 0.0f, 0.0f);
		m_ppObjects[Floorcnt]->FloorColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
		dynamic_cast<CFloorObject*>(m_ppObjects[Floorcnt])->m_IsRender = true;
		m_ppObjects[Floorcnt]->m_bPass = true;
		Floorcnt++;
	}
}

void CFloorShader::ReleaseUploadBuffers()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
			m_ppObjects[j]->ReleaseUploadBuffers();
	}
}
void CFloorShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
	CObjectsShader::Render(pd3dCommandList, pCamera);
	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j])
		{
			m_ppObjects[j]->Render(pd3dCommandList, pCamera);

		}
	}
}


/////////////////////////////////////////////////// treeshader
CTreeShader::CTreeShader()
{

}
CTreeShader::~CTreeShader()
{

}

D3D12_INPUT_LAYOUT_DESC CTreeShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);

}

D3D12_SHADER_BYTECODE CTreeShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSTree", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CTreeShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSTree", "ps_5_1", ppd3dShaderBlob));
}

void CTreeShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
	m_nPipelineStates = 1;
	m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CTreeShader::BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
	//여기불림
	CTreeMeshDiffused *pTreeMesh = new CTreeMeshDiffused(pd3dDevice, pd3dCommandList, 12.0f, 12.0f, 12.0f); 

	m_nObjects = 200;
	m_ppObjects = new CGameObject*[m_nObjects];

	CTreeObject *pTreeObject = NULL;


	for (int i = 0; i < m_nObjects; ++i)
	{
		pTreeObject = new CTreeObject();
		pTreeObject->SetMesh(pTreeMesh);
		
		if(i>= 0 && i < m_nObjects / 2)
			pTreeObject->SetPosition(-20.0f, -30.0f, i * 15.0f);
		else
			pTreeObject->SetPosition(20.0f, -30.0f, (i -  (m_nObjects / 2) ) * 15.0f);
	
		m_ppObjects[i] = pTreeObject;
	}

	CreateShaderVariables(pd3dDevice, pd3dCommandList);
}

void CTreeShader::ReleaseObjects()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
		{
			if (m_ppObjects[j])
				delete m_ppObjects[j];
		}
		delete[] m_ppObjects;
	}

}
void CTreeShader::AnimateObjects(float fTimeElapsed, CPlayer *pPlayer)
{
	CheckPass(fTimeElapsed, pPlayer);
	for (int j = 0; j < m_nObjects; j++)
	{
		m_ppObjects[j]->Animate(fTimeElapsed, pPlayer);
	}
}

void CTreeShader::ReleaseUploadBuffers()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
			m_ppObjects[j]->ReleaseUploadBuffers();
	}
}
void CTreeShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
	CShader::Render(pd3dCommandList, pCamera);
	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j])
		{
			m_ppObjects[j]->Render(pd3dCommandList, pCamera);

		}
	}
}
void CTreeShader::CheckPass(float fElapsedTime, CPlayer* pPlayer)
{
	if (pPlayer->GetPosition().z >= m_ppObjects[75]->GetPosition().z)
	{
		for (int i = 0; i < m_nObjects; ++i)
		{
			if (i >= 0 && i < m_nObjects / 2)
				m_ppObjects[i]->SetPosition(-20.0f, -30.0f, pPlayer->GetPosition().z +  i * 15.0f);
			else
				m_ppObjects[i]->SetPosition(20.0f, -30.0f, pPlayer->GetPosition().z + (i - (m_nObjects / 2) ) * 15.0f);
		}
	}
}


////////////////////////////////////star
CStarShader::CStarShader()
{

}
CStarShader::~CStarShader()
{

}

D3D12_INPUT_LAYOUT_DESC CStarShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);

}

D3D12_SHADER_BYTECODE CStarShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob)
{
	
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSStar", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CStarShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob)
{

	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSStar", "ps_5_1", ppd3dShaderBlob));
}

void CStarShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
	m_nPipelineStates = 1;
	m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CStarShader::BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
	//여기불림
	uniform_real_distribution<> ndX( -1000.0, 1000.0 );
	uniform_real_distribution<> ndZ(0.0, 10000.0);
	uniform_real_distribution<>  ndY(30, 2500.0);
	default_random_engine dre;
	CStarMeshDiffused *pStarMesh = new CStarMeshDiffused(pd3dDevice, pd3dCommandList, 8.0f, 8.0f, 8.0f); // (12.0f,12.0f,12.0f)
	m_nObjects = 3000;
	m_ppObjects = new CGameObject*[m_nObjects];
	CStarObject *pStarObject = NULL;


	for (int i = 0; i < 3000; ++i)
	{
		pStarObject = new CStarObject();
		pStarObject->SetMesh(pStarMesh);
		pStarObject->SetPosition(ndX(dre), ndY(dre),ndZ(dre));
		pStarObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
		pStarObject->SetRotationSpeed(400.0f);
		m_ppObjects[i] = pStarObject;
	}

	CreateShaderVariables(pd3dDevice, pd3dCommandList);
}

void CStarShader::ReleaseObjects()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
		{
			if (m_ppObjects[j])
				delete m_ppObjects[j];
		}
		delete[] m_ppObjects;
	}

}
void CStarShader::AnimateObjects(float fTimeElapsed, CPlayer *pPlayer)
{
	for (int j = 0; j < m_nObjects; j++)
	{
		
		m_ppObjects[j]->Animate(fTimeElapsed, pPlayer);
	}
}

void CStarShader::ReleaseUploadBuffers()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
			m_ppObjects[j]->ReleaseUploadBuffers();
	}
}
void CStarShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
	
	CShader::Render(pd3dCommandList, pCamera);
	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j])
		{
			m_ppObjects[j]->Render(pd3dCommandList, pCamera);
			
		}
	}
}

//////////////////////빌딩
CBuildingShader::CBuildingShader()
{

}
CBuildingShader::~CBuildingShader()
{

}

D3D12_INPUT_LAYOUT_DESC CBuildingShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);

}

D3D12_SHADER_BYTECODE CBuildingShader::CreateVertexShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "VSBuilding", "vs_5_1", ppd3dShaderBlob));
}

D3D12_SHADER_BYTECODE CBuildingShader::CreatePixelShader(ID3DBlob **ppd3dShaderBlob)
{
	return(CShader::CompileShaderFromFile(L"Shaders.hlsl", "PSBuilding", "ps_5_1", ppd3dShaderBlob));
}

void CBuildingShader::CreateShader(ID3D12Device *pd3dDevice, ID3D12RootSignature *pd3dGraphicsRootSignature)
{
	m_nPipelineStates = 1;
	m_ppd3dPipelineStates = new ID3D12PipelineState*[m_nPipelineStates];
	CShader::CreateShader(pd3dDevice, pd3dGraphicsRootSignature);
}

void CBuildingShader::BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList)
{
	CBuildingMeshDiffused *pBuildingMesh = new CBuildingMeshDiffused(pd3dDevice, pd3dCommandList, 12.0f, 12.0f, 12.0f);

	m_nObjects = 2500;
	m_ppObjects = new CGameObject*[m_nObjects];

	CBuildingObject **pBuildingObject = NULL;
	pBuildingObject = new CBuildingObject*[m_nObjects];
	for (int i = 0; i < m_nObjects; ++i)
	{
		pBuildingObject[i] = new CBuildingObject;
	}
	int z = 0;
	for (int i = 0; i < 50; ++i)
	{
		for (int j = 0; j < 50; ++j)
		{
			
			pBuildingObject[z]->SetMesh(pBuildingMesh);
			if (j >= 0 && j < 25)
				pBuildingObject[z]->SetPosition(-50 + j * ( - 30.0f), - 30.0f, 30 * i );
			else
				pBuildingObject[z]->SetPosition(50 + (j-25.0f) * (30.0f), -30.0f, 30 * i);
			m_ppObjects[z] = pBuildingObject[z];
			m_ppObjects[z]->BuildingColor = XMFLOAT4(urd(dreColorBuilding), urd(dreColorBuilding), urd(dreColorBuilding), 1.0f);
			z++;
		}
	}

	CreateShaderVariables(pd3dDevice, pd3dCommandList);
}

void CBuildingShader::ReleaseObjects()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
		{
			if (m_ppObjects[j])
				delete m_ppObjects[j];
		}
		delete[] m_ppObjects;
	}

}
void CBuildingShader::AnimateObjects(float fTimeElapsed, CPlayer *pPlayer)
{
	CheckPass(fTimeElapsed, pPlayer);
	for (int j = 0; j < m_nObjects; j++)
	{
		m_ppObjects[j]->Animate(fTimeElapsed, pPlayer);
	}
}

void CBuildingShader::ReleaseUploadBuffers()
{
	if (m_ppObjects)
	{
		for (int j = 0; j < m_nObjects; j++)
			m_ppObjects[j]->ReleaseUploadBuffers();
	}
}
void CBuildingShader::Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera)
{
	CShader::Render(pd3dCommandList, pCamera);
	for (int j = 0; j < m_nObjects; j++)
	{
		if (m_ppObjects[j])
		{
			m_ppObjects[j]->Render(pd3dCommandList, pCamera);

		}
	}
}

void CBuildingShader::CheckPass(float fElapsedTime, CPlayer* pPlayer)
{
	if (pPlayer->GetPosition().z >= m_ppObjects[1850]->GetPosition().z)
	{
		int z = 0;
		for (int i = 0; i < 50; ++i)
		{
			for (int j = 0; j < 50; ++j)
			{

				if (j >= 0 && j < 25)
					m_ppObjects[z]->SetPosition(-50 + j * (-30.0f), -30.0f, 30 * i + pPlayer->GetPosition().z);
				else
					m_ppObjects[z]->SetPosition(50 + (j - 25.0f) * (30.0f), -30.0f, 30 * i + pPlayer->GetPosition().z);
				z++;
			}
		}
	}
}