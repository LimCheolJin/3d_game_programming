#pragma once

const ULONG MAX_SAMPLE_COUNT = 50; // 50회의 프레임 처리시간을 누적하여 평균한다.
//프레임레이트의 계산은 일정횟수 동안 각 프레임을 렌더링 하는 시간을 평균하여 계산한다. 
class CGAMETimer
{
private:
	bool m_bHardwareHasPerformanceCounter; //컴퓨터가 performance counter 를 가지고있는가?
	float m_fTimeScale; // scale counter 의 양
	float m_fTimeElapsed;//마지막 프레임 이후 지나간 시간

	__int64 m_nCurrentTime; // 현재시간
	__int64 m_nLastTime;//마지막 프레임 시간

	__int64 m_nBaseTime;
	__int64 m_nPausedTime; 
	__int64 m_nStopTime;

	__int64 m_nPerformanceFrequency;//컴퓨터의 performance frequency

	float m_fFrameTime[MAX_SAMPLE_COUNT];//프레임 시간을 누적하기 위한 배열
	ULONG m_nSampleCount; // 누적된 프레임 횟수

	unsigned long m_nCurrentFrameRate; // 현재의 프레임 레이트
	unsigned long m_nFramesPerSecond; //초당 프레임 수
	float m_fFPSTimeElapsed; // 프레임레이트 계산 소요 시간

	bool m_bStopped;
public:
	CGAMETimer();
	virtual~CGAMETimer(); // 가상 소멸자

	void Tick(float fLockFPS = 0.0f); // 타이머의 시간을 갱신한다.
	void Start();
	void Stop();
	void Reset();
	unsigned long GetFrameRate(LPTSTR lpszString = NULL, int nCharacters = 0);
	// 프레임레이트를 반환.
	float GetTimeElapsed(); // 프레임의 평균 경과 시간을 반환. 
	float GetTotalTime(); 
};

