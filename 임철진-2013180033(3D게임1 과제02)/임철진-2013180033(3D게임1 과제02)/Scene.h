#pragma once
#include "Shader.h"

class CScene
{
public:
	CScene();
	~CScene();

	//씬에서 마우스와 키보드 메시지를 처리한다.
	bool OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	bool OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);


	void BuildObjects(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList);
	void ReleaseObjects();

	bool ProcessInput(UCHAR *pKeysBuffer);
	void AnimateObjects(float fTimeElapsed, CPlayer *pPlayer);
	void Render(ID3D12GraphicsCommandList *pd3dCommandList, CCamera *pCamera);

	void ReleaseUploadBuffers();

	ID3D12RootSignature *CreateGraphicsRootSignature(ID3D12Device *pd3dDevice); 
	ID3D12RootSignature *GetGraphicsRootSignature();

	ID3D12PipelineState *m_pd3dPipelineState;
	//파이프라인 상태를 나타내는 인터페이스 포인터이다. 

protected:
	//배치처리를 하기위하여 씬을 세이더들의 리스트로 표현함.
	CObjectsShader **m_pShaders = NULL;
	int m_nShaders = 0;


	ID3D12RootSignature *m_pd3dGraphicsRootSignature = NULL;

	//루트시그니쳐를 나타내는 인터페이스 포인터이다.
	/* 루트 시그니쳐는 렌더링 파이프라인에 묶여야하는 자원이 무엇이며(= 상수버퍼인지, 텍스쳐인지
	, 샘플러인지..) 또한 이 자원들이 입력 레지스터에 어떻게 대응되는지를 정의해야한다.
	*/
	
};

